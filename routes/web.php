<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route root ke halaman participant index
Route::get('/', 'ParticipantController@index');

Route::get('/participant', 'ParticipantController@index');
Route::get('/participant/create', 'ParticipantController@create');
Route::post('/participant', 'ParticipantController@store');
Route::get('/participant/{participant_id}/edit', 'ParticipantController@edit');
Route::put('/participant/{participant_id}', 'ParticipantController@update');
Route::delete('/participant/{participant_id}', 'ParticipantController@destroy');
Route::get('/participant/{participant_id}', 'ParticipantController@show');
Route::get('/participant/nametag/{participant_id}', 'ParticipantController@nametag');
Route::get('/participant/certificate/{participant_id}', 'ParticipantController@certificate');
