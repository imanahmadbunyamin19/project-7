@section('judul')
Edit Data Participant
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
        <form action="/participant/{{$participant->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="fullname">Full Name </label>
                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Participant Full Name" value="{{$participant->fullname}}" autofocus>
                @error('fullname')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bussiness">Bussiness Name </label>
                <input type="text" class="form-control" name="bussiness" id="bussiness" placeholder="Bussiness Name" value="{{$participant->bussiness}}">
                @error('bussiness')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{$participant->email}}">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group" id="only-number">
                <label for="phone">Phone Number </label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{$participant->phone}}">
                @error('phone')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <input type="hidden" class="form-control" name="nametag" id="nametag" value="{{$participant->nametag}}" >
            </div>

            <div class="form-group">
                <input type="hidden" class="form-control" name="certificate" id="certificate" value="{{$participant->certificate}}" >
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
</div>
@endsection
