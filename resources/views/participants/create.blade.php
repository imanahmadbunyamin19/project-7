@section('judul')
Data Participant
@endsection

@extends('template.template')

@push('script')
<script>
    // Input only number
    $(function() {
      $('#only-number').on('keydown', '#phone', function(e){
          -1!==$
          .inArray(e.keyCode,[46,8,9,27,13,110,190]) || /65|67|86|88/
          .test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey)
          || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey|| 48 > e.keyCode || 57 < e.keyCode)
          && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
      });
    })
</script>
@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">
        <form action="/participant" method="POST">
            @csrf
            <div class="form-group">
                <label for="fullname">Full Name </label>
                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Participant Full Name" value="{{old('fullname')}}" autofocus>
                @error('fullname')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="bussiness">Bussiness Name </label>
                <input type="text" class="form-control" name="bussiness" id="bussiness" placeholder="Bussiness Name" value="{{old('bussiness')}}">
                @error('bussiness')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label for="email">Email </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{old('email')}}">
                @error('email')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            
            <div class="form-group" id="only-number">
                <label for="phone">Phone Number </label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{old('phone')}}" maxlength="13">
                @error('phone')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            {{-- hidden form for status nametag and certificate --}}
            <div class="form-group">
                <input type="hidden" class="form-control" name="nametag" id="nametag" value="0" >
            </div>

            <div class="form-group">
                <input type="hidden" class="form-control" name="certificate" id="certificate" value="0" >
            </div>
            {{-- hidden form for status nametag and certificate --}}
            
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
</div>
@endsection
