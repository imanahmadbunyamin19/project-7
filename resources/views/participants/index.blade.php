@section('judul')
Data Participants
@endsection

@extends('template.template')

@push('script')

{{-- Place for Javascript Library / Custom --}}

    {{-- Data table  --}}
    <script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
    <script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>

    {{-- Sweet alert --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script type="text/javascript">
     
     $('.show_confirm').click(function(event) {
          var form =  $(this).closest("form");
          var name = $(this).data("name");
          event.preventDefault();
          swal({
              title: `Are you sure you want to delete this participant?`,
              text: " this action cannot be undone ",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              form.submit();
            }
          });
      });
  
    </script>


    {{-- Refresh Page on Print Button --}}
    <script>
    function open_in_new_tab_and_reload(url)
    {
      //Open in new tab
      window.open(url, '_blank');
      //focus to thet window
      window.focus();
      //reload current page
      location.reload();
    }
    </script>
    

@endpush

@push('style')
{{-- library, css, etc, Here! --}}

    {{-- datatables --}}
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
<div class="mx-2">
    <a href="/participant/create" class="btn btn-primary mb-3">Add New Participant</a>
    <div class="card">
        <div class="card-body">
        <table id="example1" class="table table-striped">
            <thead>
                <tr>
                    <th width="1%">No</th>
                    <th>Full Name</th>
                    <th>Bussiness Name</th>
                    <th>Phone Number</th>
                    <th>Name Tag</th>
                    <th>Certificate</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($participants as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->fullname}}</td>
                        <td>{{$value->bussiness}}</td>
                        <td>{{$value->phone}}</td>
                        <td>
                            @if($value->nametag =='0')   
                                <a href="/participant/nametag/{{$value->id}}" class="btn btn-primary btn-sm" target="_blank" onclick="open_in_new_tab_and_reload('/participant/nametag/{{$value->id}}')"> Print</a>
                            @else
                                <strong style="color:red;"> Printed </strong>
                            @endif
                        </td>
                        <td>
                            @if($value->certificate =='0')   
                                <a href="/participant/certificate/{{$value->id}}" class="btn btn-primary btn-sm" target="_blank" onclick="open_in_new_tab_and_reload('/participant/certificate/{{$value->id}}')">Print</a>
                            @else
                                <strong style="color:red;"> Printed </strong>
                            @endif
                        </td>
                        <td >
                            <form action="/participant/{{$value->id}}" method="POST">
                                <a href="/participant/{{$value->id}}" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                                <a href="/participant/{{$value->id}}/edit" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-danger show_confirm" data-toggle="tooltip" title='Delete' >
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </form>
                        </td>  
                    </tr>
                @empty
                    <tr>
                        <td colspan="6" class="text-center">No data found</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
        </div>
    </div>
</div>


@endsection