@section('judul')
Show Data Participant
@endsection

@extends('template.template')

@push('script')

@endpush

@push('style')

@endpush

@section('content')
<div class="mx-2">


    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="fullname">Full Name </label>
                <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Participant Full Name" value="{{$participant->fullname}}" readonly>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="bussiness">Bussiness Name </label>
                <input type="text" class="form-control" name="bussiness" id="bussiness" placeholder="Bussiness Name" value="{{$participant->bussiness}}" readonly>
            </div>    
        </div>
    </div>      

    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="email">Email </label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="{{$participant->email}}" readonly>
            </div>
        </div>
        <div class="col">
            <div class="form-group" id="only-number">
                <label for="phone">Phone Number </label>
                <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone Number" value="{{$participant->phone}}" readonly>
            </div>
        </div>
    </div>
       
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label for="nametag">Name Tag Print Status </label>
                <input type="text" class="form-control" name="nametag" id="nametag" placeholder="nametag"
                    @if($participant->nametag =='0')   
                        value="Not yet"
                    @else
                        value="Printed"
                    @endif
                readonly>
            </div>
        </div>
        <div class="col">
            <div class="form-group">
                <label for="certificate">Certificate Print Status </label>
                <input type="text" class="form-control" name="certificate" id="certificate" placeholder="certificate"
                    @if($participant->certificate =='0')   
                        value="Not yet"
                    @else
                        value="Printed"
                    @endif
                readonly>
            </div>
        </div>
    </div>
    
</div>
@endsection
