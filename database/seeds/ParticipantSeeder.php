<?php

use Illuminate\Database\Seeder;
use App\Participants;

class ParticipantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Participants::create([
            'fullname' => 'Iman Ahmad Bunyamin',
            'bussiness' => 'IA Corps',
            'email' => 'imanahmadb@yahoo.co.id',
            'phone' => '085294194777',
            'nametag' => 0,
            'certificate' => 0
        ]);

        Participants::create([
            'fullname' => 'Herma SR',
            'bussiness' => 'IA Corps',
            'email' => 'hermasr@yahoo.co.id',
            'phone' => '085294194666',
            'nametag' => 0,
            'certificate' => 0
        ]);
    }
}
