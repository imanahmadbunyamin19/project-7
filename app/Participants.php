<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    protected $table = 'participants';
    protected $fillable = ['fullname', 'bussiness', 'email', 'phone', 'nametag', 'certificate'];
}
