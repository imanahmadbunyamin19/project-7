<?php

namespace App\Http\Controllers;

use App\Participants;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;
class ParticipantController extends Controller
{
    // Index Page
    public function index()
    {
        $participants = Participants::all();
        return view('participants.index', compact('participants'));
    }

    // Create 
    public function create()
    {
        return view('participants.create');
    }

    // Save or store functions
    public function store(Request $request)
    {
        // Validations
        $request -> validate(
            [
                'fullname' => 'required',
                'bussiness' => 'required',
                'email' => 'required',
                'phone' => 'required',
            ]
        );

        // Insert New Data
        DB::table('participants')->insert(
            [
                'fullname' => $request['fullname'],
                'bussiness' => $request['bussiness'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'nametag' => $request['nametag'],
                'certificate' => $request['certificate'],
            ]
        );
        return redirect('/participant');
    }

    // to Edit Form
    public function edit($id)
    {
        $participant = Participants::findOrFail($id);
        return view('participants.edit', compact('participant'));
    }

    // Update function
    public function update(Request $request, $id)
    {
        // Validations
        $request -> validate(
            [
                'fullname' => 'required',
                'bussiness' => 'required',
                'email' => 'required',
                'phone' => 'required',
            ]
        );

        // Do Update
        $participant = Participants::find($id);

        $participant -> fullname = $request-> fullname;
        $participant -> bussiness = $request-> bussiness;
        $participant -> email = $request-> email;
        $participant -> phone = $request-> phone;      

        $participant -> save();

        return redirect('/participant');
    }

    // to show
    public function show($id)
    {
        $participant = Participants::findOrFail($id);
        return view('participants.show', compact('participant'));
    }

    // delete or destroy
    public function destroy($id)
    {
        $participant = Participants::find($id);
        $participant->delete();

        return redirect('/participant')->with('Success', 'Data deleted successfully');;
    }
    
    // print Nametag
    public function nametag($id)
    {

        // update status : print nametag (to printed / to boolean 1)
        $participant = Participants::find($id);
        $participant -> nametag = 1;  
        $participant -> save();

        // proceed to print nametag
        $participants = Participants::find($id);
        $pdf = PDF::loadview('print.nametag',compact('participants'))->setPaper('B5', 'portait');
    	return $pdf->stream('');

    }

    // print Certificate
    public function certificate($id)
    {
        // update status : print certificate (to printed / to boolean 1)
        $participant = Participants::find($id);
        $participant -> certificate = 1;  
        $participant -> save();

        // proceed to print certificate
        $participants = Participants::find($id);
        $pdf = PDF::loadview('print.certificate',compact('participants'))->setPaper('A4', 'landscape');
    	return $pdf->stream('');
    }

}
